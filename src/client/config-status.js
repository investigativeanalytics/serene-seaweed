module.exports = {
  CONFIG_STATUS_NORMAL: 'CONFIG_STATUS_NORMAL',
  CONFIG_STATUS_CHANGED: 'CONFIG_STATUS_CHANGED',
  CONFIG_STATUS_SAVING: 'CONFIG_STATUS_SAVING',
};
