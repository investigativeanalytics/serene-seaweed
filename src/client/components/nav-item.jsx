const React = require('react');

module.exports = ({children}) =>
  <div className="navbar-item">{children}</div>;
