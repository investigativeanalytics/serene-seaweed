const React = require('react');

const FullView = ({children}) => {
  return (
    <div className="view">
      {children}
    </div>
  );
};

module.exports = FullView;
