const classesSelector = (state) => state.getIn(['ui', 'graphSchemaClasses']);

module.exports = {classesSelector};
