const classLinksSelector = (state) => state.getIn(['ui', 'graphSchemaClassLinks']);

module.exports = {classLinksSelector};
